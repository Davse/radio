var document = document || {},
    RedesSociales = {
        iniciar: function(){
            document.getElementsByTagName("header")[0].appendChild(this.generarMenuRedes())		
        },
        
        generarMenuRedes: function(){
            var eUl,eLi1,eLi2,eLi3,eA1,eA2,eA3,eImg1,eImg2,eImg3,boxRedes,h1;
	    boxRedes = document.createElement("section");
	    boxRedes.setAttribute("id","davsecRedes");
	    h1 = document.createElement("h1");
	    h1.setAttribute("id","davtituloRedes");
	    h1.textContent = "Seguinos";			
            eUl = document.createElement("ul");
            eLi1 = document.createElement("li");
            eLi2 = document.createElement("li");
            eLi3 = document.createElement("li");
            eA1 =document.createElement("a");
            eA2 =document.createElement("a");
            eA3 =document.createElement("a");
            eImg1 =document.createElement("img");
            eImg2 =document.createElement("img");
            eImg3 =document.createElement("img");
            eLi1.setAttribute("class","davli");
            eLi2.setAttribute("class","davli");
            eLi3.setAttribute("class","davli");
            eUl.setAttribute("class","davul");
            eImg1.setAttribute("class","davimg");
            eImg2.setAttribute("class","davimg");
            eImg3.setAttribute("class","davimg");
            eImg1.setAttribute("src","redes/facebook.png");
            eImg2.setAttribute("src","redes/instagram.png");
            eImg3.setAttribute("src","redes/twitter.png");
            eA1.setAttribute("href","https://www.facebook.com/laguiadelocio");
            eA2.setAttribute("href","https://twitter.com/LaGuiadelOcio");
            eA3.setAttribute("href","https://www.instagram.com/laguiadelocioar");
            eA1.setAttribute("target","_blank");
            eA2.setAttribute("target","_blank");
            eA3.setAttribute("target","_blank");
            eA1.appendChild(eImg1);
            eA2.appendChild(eImg2);
            eA3.appendChild(eImg3);
            eLi1.appendChild(eA1);
            eLi2.appendChild(eA2);
            eLi3.appendChild(eA3);
            eUl.appendChild(eLi1);
            eUl.appendChild(eLi2);
            eUl.appendChild(eLi3);
	    boxRedes.appendChild(h1);
	    boxRedes.appendChild(eUl);
            return boxRedes;
        },   
        
    }
