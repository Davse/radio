var document = document || {},
    reproductor = document.createElement("audio"),
    Radio = {
            iniciarRadio:function(){
                document.getElementsByClassName("main-player")[0].appendChild(Radio.constructor());
                document.getElementsByTagName("header")[0].appendChild(Radio.constructor());
                window.onscroll = function() {Radio.scrollRadio()};
                Radio.menuMobile();
            },
            constructor: function(){
              var control = document.createElement("div"),boton = document.createElement("div");
                reproductor.controls = false;
                reproductor.setAttribute("src","http://mp3.metroaudio1.stream.avstreaming.net:7200/metro");
                control.setAttribute("class","control");
                boton.setAttribute("class","boton");
                boton.classList.add("play");
                control.addEventListener("click",Radio.play_pause);
                control.appendChild(reproductor);
                control.appendChild(boton);
                return control;
        },
            play_pause:function(){
            if(reproductor.paused){    
              reproductor.play();
              document.getElementsByClassName("boton")[0].classList.remove("play");
              document.getElementsByClassName("boton")[1].classList.remove("play");
              document.getElementsByClassName("boton")[0].classList.add("pause");
              document.getElementsByClassName("boton")[1].classList.add("pause");
            }else{
               reproductor.pause();
              document.getElementsByClassName("boton")[0].classList.remove("pause");
              document.getElementsByClassName("boton")[1].classList.remove("pause");    
              document.getElementsByClassName("boton")[0].classList.add("play");
              document.getElementsByClassName("boton")[1].classList.add("play");  
            }    
            },
            scrollRadio: function() {
              var botonBarra = document.getElementsByClassName("control")[0],texto=document.getElementById("menulive");
              if (document.body.scrollTop > document.getElementsByClassName("boton")[1].getBoundingClientRect().top || document.documentElement.scrollTop > document.getElementsByClassName("boton")[1].getBoundingClientRect().top) {
                botonBarra.style.display="block";
                texto.style.display="block";
              } else {
                botonBarra.style.display="none";
                texto.style.display="none";  
              }
            },
            menuMobile: function(){
                var menu = document.getElementsByClassName("davMenu-toggle")[0];
                menu.addEventListener("click",Radio.open);
            },
            open: function(){
               this.classList.add("open");
               this.removeEventListener("click",Radio.open);    
               this.addEventListener("click",Radio.close);    
                document.getElementById("davsecRedes").style.display="block";
            },
            close: function(){
                this.classList.remove("open");
                this.removeEventListener("click",Radio.close);
                this.addEventListener("click",Radio.open);
                document.getElementById("davsecRedes").style.display="none";
            },
            
    }
