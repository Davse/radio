var document = document || {},
    audio,
    arch,
    Repruductor ={
        constructor:function(x){
          Repruductor.loadAudio(x);  
            
        },
        PlayPause: function(){
            var btPlayPause = document.createElement("div"), iconoPlayPause = document.createElement("p");
            btPlayPause.setAttribute("class","player-btn play-pause");
            iconoPlayPause.setAttribute("class","icon-i_play icon-size hover-d");
            iconoPlayPause.textContent="P";
            btPlayPause.addEventListener("click",Repruductor.stop);
            btPlayPause.appendChild(iconoPlayPause);
            return btPlayPause;
        },
        stop: function(e){
            var targ = audio,iniciar;
            if(!targ.paused){
                targ.pause();
                e.target.firstChild.textContent="P";
                clearInterval(iniciar);
            }else{
              targ.play();
              e.target.firstChild.textContent="p";
            iniciar= setInterval(function(){document.getElementsByClassName("player-progress-value")[0].style.width = (audio.currentTime * 100)/audio.duration+"%";if(audio.ended){document.getElementsByClassName("icon-i_play icon-size hover-d")[0].textContent="P";
                clearInterval(iniciar);}},700);    
            }
            
        },
        BarraProgreso: function(){
            var contenedor = document.createElement("div"), barraProgreso = document.createElement("div"),valor = document.createElement("span");
           contenedor.setAttribute("class","player-progress-container");
           barraProgreso.setAttribute("class","player-progress");
            valor.setAttribute("class","player-progress-value");
            valor.style.width= "0%";
            Repruductor.AudioScroll(valor);
            barraProgreso.appendChild(valor);
            contenedor.appendChild(barraProgreso);
            return contenedor;
        },
        volumen: function(){
            var btvolumen = document.createElement("div"), iconoVolumen = document.createElement("p");
            btvolumen.setAttribute("class","player-btn volume");
            btvolumen.setAttribute("title","Mute/Unmute");
            iconoVolumen.setAttribute("class","icon-size hover-d icon-i_volume");
            iconoVolumen.textContent="v";
            btvolumen.addEventListener("click",Repruductor.mute);
            btvolumen.appendChild(iconoVolumen);
            return btvolumen;
        },
        mute:function(e){
           var targ =audio;
           if(!targ.muted){    
            targ.muted=true;
            e.target.firstChild.textContent="m"; 
           }else{
             targ.muted=false;
             e.target.firstChild.textContent="v";   
           }       
        },
        descarga: function(){
           var btdescarga = document.createElement("div"),size = document.createElement("a"), iconodescarga = document.createElement("p");
            btdescarga.setAttribute("class","player-btn download");
            btdescarga.setAttribute("title","Descargar audio");
            size.setAttribute("class","icon-size");
            size.setAttribute("href",arch['audioUrl']);
            size.setAttribute("download","fileName.mp3");
            iconodescarga.setAttribute("class","icon-i_download icon-size hover-d");
            iconodescarga.textContent="d";
            size.appendChild(iconodescarga);
            btdescarga.appendChild(size);
            return btdescarga;
        },
        
        AudioScroll : function(elm) { 
            var iniciar;
		elm.addEventListener("mousedown", function(){  
			document.onmousemove = function(e) {
                audio.pause();
                clearInterval(iniciar);
			  Repruductor.setPlayProgress( e.pageX,elm); 
			}
			 document.onmouseup = function(e) {
                 var selector=document.getElementsByClassName("icon-i_play icon-size hover-d")[0];
                 audio.play();
                 selector.textContent="p";
                 iniciar= setInterval(function(){document.getElementsByClassName("player-progress-value")[0].style.width = (audio.currentTime * 100)/audio.duration+"%";if(audio.ended){selector.textContent="P";
                clearInterval(iniciar);}},1000);    
				document.onmouseup = null; 
				document.onmousemove = null; 
				Repruductor.setPlayProgress( e.pageX,elm );
			}
		}, true);
            
            elm.addEventListener("touchstart", function(){ 
			this.ontouchmove = function(e) {   
                audio.pause();
                clearInterval(iniciar);
			  Repruductor.setPlayProgress( e.changedTouches[0].pageX,elm); 
			}
			 this.ontouchend = function(e) {
                 var selector=document.getElementsByClassName("icon-i_play icon-size hover-d")[0];
                 audio.play();
                 selector.textContent="p";
                 iniciar= setInterval(function(){document.getElementsByClassName("player-progress-value")[0].style.width = (audio.currentTime * 100)/audio.duration+"%";if(audio.ended){selector.textContent="P";
                clearInterval(iniciar);}},1000);    
				document.onmouseup = null; 
				document.onmousemove = null; 
				Repruductor.setPlayProgress( e.changedTouches[0].pageX,elm );
			}
		}, true);
	},
	 
	setPlayProgress : function( clickX,x ) { 
		var newPercent =Math.max( 0, Math.min(100,((clickX - this.findPosX(x)) / document.getElementsByClassName("player-progress")[0].offsetWidth)*100));
		audio.currentTime = Math.trunc((newPercent * audio.duration)/100); 
		x.style.width = newPercent  + "%"; 
	}, 
	 
	findPosX : function(progressHolder) { 
		var curleft = progressHolder.offsetLeft; 
		while( progressHolder = progressHolder.offsetParent ) { 
			curleft += progressHolder.offsetLeft; 
		} 
		return curleft; 
	},
     objAudio: function(Aud){
         var articulo = document.createElement("article"),contenedor = document.createElement("div"), opciones = document.createElement("div"),titulo = document.createElement("h3"), leyenda = document.createElement("p");
            audio = document.createElement("audio");
            audio.controls = false;
            audio.setAttribute("src",Aud['audioUrl']);
            articulo.setAttribute("class","audioObj");
            articulo.setAttribute("id",Aud['audioUrl']);
            titulo.setAttribute("class","tituloArticulo");
            leyenda.setAttribute("class","leyendaArticulo");
            titulo.textContent=Aud['titulo'];
            leyenda.textContent=Aud['leyenda'];
            contenedor.setAttribute("class","player-container");
            opciones.setAttribute("class","player-options");
            opciones.appendChild(Repruductor.PlayPause());
            opciones.appendChild(Repruductor.BarraProgreso());
            opciones.appendChild(Repruductor.volumen());
            opciones.appendChild(Repruductor.descarga());
            contenedor.appendChild(audio);
            contenedor.appendChild(opciones);
            articulo.appendChild(titulo);
            articulo.appendChild(leyenda);
            articulo.appendChild(contenedor);
         return articulo;
     },   
    loadAudio: function(x) {
            var archivo = new XMLHttpRequest(),url ="jsonAudio/prueba.json";
            archivo.open('POST', url, true);
            archivo.responseType = 'json';
            archivo.onload = function() {
               arch= archivo.response;
               document.querySelector(x).appendChild(Repruductor.objAudio(arch));    
            };
            archivo.send();
        },    
        
        
    }
